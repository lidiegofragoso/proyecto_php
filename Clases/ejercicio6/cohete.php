<?php 
include_once('transporte.php'); 
class cohete extends transporte{

private $numero_turbinas;

//sobreescritura de constructor
public function __construct($nom,$vel,$com,$turb){
    parent::__construct($nom,$vel,$com);
    $this->numero_turbinas=$turb;
}

// sobreescritura de metodo
public function resumenCohete(){
    $mensaje=parent::crear_ficha();
    $mensaje.='<tr>
                <td>Numero de turbinas:</td>
                <td>'. $this->numero_turbinas.'</td>				
            </tr>';
    return $mensaje;
    }
}
?>