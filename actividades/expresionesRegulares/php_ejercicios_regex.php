//Realizar una expresión regular que detecte emails correctos.
'/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})$/'

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
'/^([A-Z0-9]{1,18})$/'


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
'/^([A-z]{1,50})$/'

//Crea una funcion para escapar los simbolos especiales.
'/^([^A-z0-90])$/'

//Crear una expresion regular para detectar números decimales.
'/^[0-9]\.[0-9]$/'
