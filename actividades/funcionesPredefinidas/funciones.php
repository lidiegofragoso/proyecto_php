<?php
//substr: Devuelve parte de una cadena
$string1="Welcome to las Vegas, hope enjoy your visit";
echo $string1;
echo '<br>';
echo substr($string1,1);
echo '<br>';
echo substr($string1,1,5);
echo '<br>';
echo substr($string1,0,10);
echo '<br>';
echo substr($string1,-1,1);
echo '<br>';

//strstr: Se usa para obtener la primera aparición de una cadena dentro de otra cadena.
$string1="Google.com";
$newstring=strstr($string1,".");
echo $newstring;
//strpos: Encuentra la posición de la primera ocurrencia de un substring en un string
$main_string='google.com';
echo strrpos($main_string, 'com');
//implode: Une elementos de un array en un string
$array_name=array('First Name', 'Middle Name', 'Last Name');
$join_string=implode("-", $array_name);
echo $join_string;
echo '<br>';
$join_string=implode(" ", $array_name);
echo $join_string;
//explode: Divide un string en varios string
$class_list='V,VI,VII,VIII,IX,X';
$classes=explode(",",$class_list);
print_r($classes);
/*utf8_encode: Ésta función codifica el string data a UTF-8, y devuelve una versión codificada. UTF-8 es un mecanismo estándar usado por Unicode para la codificación de los valores de caracteres anchos en un flujo de bytes. 
utf8_encode ( string $data ) : string
utf8_decode: Ésta función decodifica los data, asumidos a ser codificados por UTF-8, a ISO-8859-1.
utf8_decode ( string $data ) : string*/
//array_pop:  extrae y devuelve el último valor del array, acortando el array con un elemento menos.
$stack = array("naranja", "plátano", "manzana", "frambuesa");
$fruit = array_pop($stack);
print_r($stack);
//array_push: array_push() trata array como si fuera una pila y coloca la variable que se le proporciona al final del array. 
$pila = array("naranja", "plátano");
array_push($pila, "manzana", "arándano");
print_r($pila);
/*array_diff: Compara array1 con uno o más arrays y devuelve los valores de array1 que no estén presentes en ninguno de los otros arrays.
array_diff ( array $array1 , array $array2 [, array $... ] ) : array
//array_walk: Aplicar una función proporcionada por el usuario a cada miembro de un array
array_walk ( array &$array , callable $callback [, mixed $userdata = NULL ] ) : bool*/
//sort: Ordena un array
$frutas = array("limón", "naranja", "banana", "albaricoque");
sort($frutas);
foreach ($frutas as $clave => $valor) {
    echo "frutas[" . $clave . "] = " . $valor . "\n";
}
//current: Devuelve el elemento actual en un array
$transport = array('pie', 'bici', 'coche', 'avión');
$mode = current($transport); // $mode = 'pie';
$mode = next($transport);    // $mode = 'bici';
$mode = current($transport); // $mode = 'bici';
$mode = prev($transport);    // $mode = 'pie';
$mode = end($transport);     // $mode = 'avión';
$mode = current($transport); // $mode = 'avión';

$arr = array();
var_dump(current($arr)); // bool(false)

$arr = array(array());
var_dump(current($arr)); // array(0) { }
//date: Dar formato a la fecha/hora local
// Imprime algo como: 2000-07-01T00:00:00+00:00
echo date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000));
//empty: Determina si una variable está vacía
$var = 0;

// Se evalúa a true ya que $var está vacia
if (empty($var)) {
    echo '$var es o bien 0, vacía, o no se encuentra definida en absoluto';
}
//isset: Determina si una variable está definida y no es NULL
$a = array ('test' => 1, 'hello' => NULL, 'pie' => array('a' => 'apple'));

var_dump(isset($a['test']));            // TRUE
var_dump(isset($a['foo']));             // FALSE
var_dump(isset($a['hello']));           // FALSE

// La clave 'helo' es igual a NULL así que no se considera definida
// Si desea comprobar los valores NULL clave, intente:
var_dump(array_key_exists('hello', $a)); // TRUE

// Comprobando valores de arrays con más profunidad
var_dump(isset($a['pie']['a']));        // TRUE
var_dump(isset($a['pie']['b']));        // FALSE
var_dump(isset($a['cake']['a']['b']));  // FALSE
/*serialize: Genera una representación apta para el almacenamiento de un valor
// $datos_sesion contiene un array multi-dimensional con
// información del usuario actual. Usamos serialize() para
// almacenarla en una base de datos al final de la petición.

$con  = odbc_connect("bd_web", "php", "gallina");
$sent = odbc_prepare($con,
      "UPDATE sesiones SET datos = ? WHERE id = ?");
$datos_sql = array (serialize($datos_sesion), $_SERVER['PHP_AUTH_USER']);

if (!odbc_execute($sent, &$datos_sql)) {
    $sent = odbc_prepare($con,
     "INSERT INTO sesiones (id, datos) VALUES(?, ?)");
    if (!odbc_execute($sent, &$datos_sql)) {
        /* Algo ha fallado.. 
    }
}
*/
//unserialize: Crea un valor PHP a partir de una representación almacenada
$objeto_seriado='O:1:"a":1:{s:5:"valor";s:3:"100";}';

ini_set('unserialize_callback_func', 'mi_llamada_de_retorno'); // defina su callback_function

function mi_llamada_de_retorno($nombre_clase)
{
    // tan solo incluya un fichero que contenga su definición de clase
    // recibe $nombre_clase para determinar qué definición de clase requiere
}
?>