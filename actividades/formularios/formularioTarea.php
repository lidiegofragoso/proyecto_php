<!doctype html>
<html lang="en" class="h-100">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Estilo -->
  <link rel="stylesheet" href="css/estilo.css">

  <!-- Fuentes -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet"> 

  <title>Formulario</title>
  

</head>
<body class="d-flex flex-column h-100">

  <!-- Contenido -->
  <main role="main" class="flex-shrink-0">

    <div class="container m-top">

      <h1>Registro de alumno</h1>

      <hr>
      
      <!-- Formulario -->
      <form>
        
        <!-- Número de cuenta -->
        <div class="form-group row">
          <label for="numeroCuenta" class="col-md-2 col-form-label">Número de cuenta:</label>
          <div class="col-md-10">
            <input type="numeric" class="form-control" id="numeroCuenta">
          </div>
        </div>
        
        <!-- Nombre -->
        <div class="form-group row">
          <label for="nombre" class="col-md-2 col-form-label">Nombre: </label>
          <div class="col-md-10">
            <input type="text" class="form-control" id="nombre">
          </div>
        </div>
        
        <!-- Primer apellido -->
        <div class="form-group row">
          <label for="primerApellido" class="col-md-2 col-form-label">Primer apellido:</label>
          <div class="col-md-10">
            <input type="text" class="form-control" id="primerApellido">
          </div>
        </div>
        
        <!-- Segundo apellido  -->
        <div class="form-group row">
          <label for="segundoApellido" class="col-md-2 col-form-label">Segundo apellido:</label>
          <div class="col-md-10">
            <input type="text" class="form-control" id="segundoApellido">
          </div>
        </div>
        
        <!-- Contraseña -->
        <div class="form-group row">
          <label for="contrasena" class="col-md-2 col-form-label">Contraseña:</label>
          <div class="col-md-10">
            <input type="password " class="form-control" id="contrasena">
          </div>
        </div>
        
        <!-- Radios -->
        <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Género</legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
                <label class="form-check-label" for="gridRadios1">
                  M
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
                <label class="form-check-label" for="gridRadios2">
                  F
                </label>
              </div>
              <div class="form-check disabled">
                <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3">
                <label class="form-check-label" for="gridRadios3">
                  Otro
                </label>
              </div>
            </div>
          </div>
        </fieldset>

        <!-- Fecha de nacimiento  -->
        <div class="form-group row">
          <label for="segundoApellido" class="col-md-2 col-form-label">Fecha de nacimiento:</label>
          <div class="col-md-10">
            <input type="date" class="form-control" id="segundoApellido">
          </div>
        </div>
          
        <!-- Botón enviar -->
        <button type="submit" class="btn btn-primary">Enviar</button>

      </form>
    </div>
  </main>
</body>
</html>
